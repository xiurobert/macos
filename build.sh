#!/bin/sh

# base version
pkgver=96.0.1
# release number in the source repo, to fetch the source code
release=1
# release number in the osx repo, to release a new version
mac_release=1

# see the pre-requisites first
wasi_path=/usr/local/Cellar/llvm/13.0.0_2/lib/clang/13.0.0/lib

objdir=obj-*/dist/librewolf
ospkg=app
bold=$(tput bold)
normal=$(tput sgr0)


prepare_source() {

    # cleaning
    echo "[debug] cleaning.."
    rm -rf librewolf-$pkgver-$release.source.tar.gz librewolf-$pkgver
    
    # fetch
    echo "\n${bold}-> Fetching LibreWolf source code${normal}"
    wget -q --show-progress -O librewolf-$pkgver-$release.source.tar.gz https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$pkgver-$release.source.tar.gz?job=build-job
    if [ $? -ne 0 ]; then exit 1; fi
    if [ ! -f librewolf-$pkgver-$release.source.tar.gz ]; then exit 1; fi
    echo "${bold}-> Retrieved librewolf-$pkgver-$release.source.tar.gz ${normal}"

    # extract
    echo "\n${bold}-> Extracting librewolf-$pkgver-$release.source.tar.gz (might take a while)${normal}"
    tar xf librewolf-$pkgver-$release.source.tar.gz
    if [ $? -ne 0 ]; then exit 1; fi
    if [ ! -d librewolf-$pkgver ]; then exit 1; fi
    echo "${bold}-> Extracted successfully ${normal}"

    # configure
    echo "\n${bold}-> Configuring the build${normal}"
    if [ ! -d librewolf-$pkgver ]; then exit 1; fi
    cd librewolf-$pkgver
    # osx specific stuff
    patch -p1 -i ../patches/browser-branding-configure.patch
    cp -f ../icons/icns/Librewolf-3.icns browser/branding/librewolf/firefox.icns
    
    echo "ac_add_options --with-wasi-sysroot=$HOME/.mozbuild/wrlb/wasi-sysroot" >> mozconfig
    if ([[ -d ~/.mozbuild/macos-sdk/MacOSX11.3.sdk ]])
    then
        echo "ac_add_options --with-macos-sdk=$HOME/.mozbuild/macos-sdk/MacOSX11.3.sdk" >> mozconfig
        echo "${bold}-> Using SDK from .mozbuild${normal}"
    fi
    cd ..

    # xcomp() - just run it after this function.

}

xcomp() {

    # to build for M1 on x86
    cd librewolf-$pkgver
    echo "ac_add_options --target=aarch64" >> mozconfig
    echo "${bold}-> Prepared to cross-compile${normal}"
    cd ..

}

mach_build() {

    echo "\n${bold}-> OK, let's build.${normal}\n"
    if [ ! -d librewolf-$pkgver ]; then exit 1; fi
    cd librewolf-$pkgver
    
    ./mach build
    if [ $? -ne 0 ]; then exit 1; fi
    echo "\n${bold}-> The build ended successfully${normal}\n"

    ./mach package
    if [ $? -ne 0 ]; then exit 1; fi
    rm $objdir/LibreWolf.$ospkg/Contents/MacOS/pingsender
    echo "${bold}-> Packaged LibreWolf${normal}"
    
    cd ..

}

build_artifacts() {

    if [ ! -d librewolf-$pkgver/$objdir ]; then exit 1; fi
    cp -r librewolf-$pkgver/$objdir .

    echo "\n${bold}-> Creating .zip${normal}"
    zip -qr9 librewolf-$pkgver-$mac_release.zip librewolf

    echo "\n${bold}-> Creating .dmg${normal}\n"
    cd utils
    ./disk-image.sh $pkgver-$mac_release

}

# extra functions below

add_to_apps() {

    if [ ! -d librewolf ]; then exit 1; fi
    cp -r librewolf/* /Applications    
    echo "\n${bold}-> LibreWolf.app available in /Applications\n"

}

full_build() {
    prepare_source
    mach_build
    build_artifacts
}


# process commandline arguments and do something
done_something=0
if [[ "$*" == *prepare_source* ]]; then
    prepare_source
    done_something=1
fi
if [[ "$*" == *xcomp* ]]; then
    xcomp
    done_something=1
fi
if [[ "$*" == *mach_build* ]]; then
    mach_build
    done_something=1
fi
if [[ "$*" == *build_artifacts* ]]; then
    build_artifacts
    done_something=1
fi
if [[ "$*" == *add_to_apps* ]]; then
    add_to_apps
    done_something=1
fi
if [[ "$*" == *full_build* ]]; then
    full_build
    done_something=1
fi

if (( done_something == 0 )); then
    cat <<EOF

Build script for the OSX version of LibreWolf.
For more details check the build guide: https://gitlab.com/librewolf-community/browser/macos/-/blob/master/build_guide.md

${bold}BUILD${normal}

    ${bold}./build.sh${normal} command

${bold}BUILD COMMANDS${normal}

    ${bold}full_build${normal}
        The full build process.
    
EOF
    exit 1
fi
